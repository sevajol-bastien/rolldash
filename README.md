# rolldash

An android app which provide a dashboard for [rolling](https://github.com/buxx/rolling) game.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/fr.bux.rollingdashboard/)
